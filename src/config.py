#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import re
import sys

import yaml
from utilities import get_list, merge_yaml


class Config():
    def __init__(self, filename):
        self.env = {}
        self.full_path = self.get_filename(filename)
        self.filename = filename
        try:
            with open('.env', 'r') as f:
                for line in f:
                    key, value = line.strip().split('=')
                    self.env[key] = value
        except FileNotFoundError:
            pass
        with open(self.full_path, 'r') as f:
            self.data = self.postprocess(yaml.load(f, self.PiOvenLoader))

    def __repr__(self):
        return self.filename

    def dumps(self):
        return yaml.dump(self.data, indent=4, default_flow_style=False)

    def get_filename(self, path):
        """ Given a YML file, determine the full path the file. First check the local directory,
            then the defaults directory. """
        default = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'defaults', path)
        if os.path.isfile(path):
            return path
        elif os.path.isfile(default):
            return default
        else:
            raise self.PiOvenConfigException(f"The recipe {path} cannot be found.")

    def postprocess(self, data):
        ret = {}
        if 'setenv' in data:
            for pair in data['setenv']:
                for key in pair:
                    self.env[key] = pair[key]
            for key in self.env:
                if not os.environ.get(key, ''):
                    os.environ[key] = str(self.env[key])
            del data['setenv']
        if 'image' in data:
            image_default = f"{data['image']}.yml"
            if 'inherit' in data:
                data['inherit'] = get_list(data['inherit']).append(image_default)
            else:
                data['inherit'] = [image_default, ]
            del data['image']
        if 'inherit' in data:
            for filename in get_list(data['inherit']):
                full_path = self.get_filename(filename)
                with open(full_path, 'r') as f:
                    new = self.postprocess(yaml.load(f, self.PiOvenLoader))
                    ret = merge_yaml(ret, new)
            del data['inherit']
        ret = merge_yaml(ret, data)
        return ret

    class PiOvenConfigException(Exception):
        pass

    class PiOvenLoader(yaml.SafeLoader):
        env_matcher = re.compile(r'.*?\${(\w+)}.*?')

        def __init__(self, stream):
            self._root = os.path.split(stream.name)[0]
            super().__init__(stream)

        def include(self, node):
            filename = os.path.join(self._root, self.construct_scalar(node))

            with open(filename, 'r') as f:
                return self.postproccess(yaml.load(f, self.PiOvenLoader))

        def _sub_handler(self, name):
            if name.startswith('?'):
                return self.get_env(name[1:], False)
            else:
                return self.get_env(name, True)

        def env_constructor(self, node):
            value = self.construct_scalar(node)
            match = self.env_matcher.findall(value)
            if match:
                full_value = value
                for g in match:
                    full_value = full_value.replace(f'${{{g}}}', self._sub_handler(g))
                return full_value
            return value

        def get_env(self, name, required):
            ret = os.environ.get(name, '')
            if ret:
                return str(ret)
            else:
                if required:
                    raise Config.PiOvenConfigException(f"Required environment variable {name} is empty.")
                else:
                    return ''

    PiOvenLoader.add_constructor('!include', PiOvenLoader.include)
    PiOvenLoader.add_constructor('!env', PiOvenLoader.env_constructor)
    PiOvenLoader.add_implicit_resolver('!env', PiOvenLoader.env_matcher, None)


def ParseConfig(path):
    if os.path.exists(path):
        config = Config(path)
        return config

    elif not os.path.exists(path):
        print(f'The config file: {path} Doesn\'t exist!')
        sys.exit('Config file not found')
