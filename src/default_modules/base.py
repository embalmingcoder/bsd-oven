#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import crypt
import os
import re
import struct
import tempfile
import urllib.request
import zipfile
from pathlib import Path
from shutil import copy

from instructions import (CopyFile, CreateDir, CreateFile, ExecuteShell,
                          ModifyFile, SymLinkFile)


class BaseModule(object):
    required_fields = []
    default_fields = []
    default_modules = []
    module_name = 'BaseModule'

    def __init__(self, recipe, parent=None):
        self.recipe = recipe
        self.registered_modules = {module.module_name: module for module in self.default_modules}
        if not parent:
            self.root = self
            self.installs = []
            self.first_start = []
            self.instructions = []
        else:
            self.root = parent.root
        for field_name in self.default_fields:
            setattr(self, f'field_{field_name}', self.handle_default_field)

    def handle_default_field(self, recipe, field):
        setattr(self, field, recipe)
        return []

    def register_module(self, module):
        if issubclass(module, BaseModule):
            self.registered_modules[module.module_name] = module
        else:
            raise self.NotAModuleException(f"Attempted to register {module}- but it isn't a module.")

    def create_instruction(self):
        return []

    def add_program(self, program):
        self.root.installs.append(program)

    def generate_first_start(self):
        invocation = self.root.recipe['_install_invocation']
        sep = self.root.recipe['_install_seperator']
        contents = "#!/bin/sh\nif [ -f /usr/local/etc/first_start ]; then"
        contents += "\n\texit 0\nelse\n\t/bin/touch /usr/local/etc/first_start\nfi\n"
        contents += invocation.format(**{'programs': sep.join(self.installs)}) + '\n'
        contents += '\n'.join(self.first_start) + '\n'
        return [
            ModifyFile('/etc/rc.local', '^exit 0$', '/usr/local/bin/first_start.sh\nexit 0'),
            CreateFile('/usr/local/bin/first_start.sh', contents, permissions=0o744),
        ]

    def handle_modules(self):
        errors = []
        fields = {func: getattr(self, func) for func in dir(self) if func.startswith('field_')}
        missing_fields = list(set(self.required_fields) - set(fields.keys()))
        for missing_field in missing_fields:
            errors.append(f"CRITICAL: {self.__class__.__name__} is missing required field {missing_field}.")
        if missing_fields:
            return errors
        for key in self.recipe:
            if key in self.registered_modules:
                sub = self.registered_modules[key](self.recipe[key], self)
                # register any discovered submodules for sub here.
                errors += sub.handle_modules()
            elif f"field_{key}" in fields:
                errors += fields[f"field_{key}"](self.recipe[key], field=key)
            else:
                if key.startswith('_'):
                    continue
                errors.append((f"{self.__class__.__name__} could find no registered modules for {key}. "
                               f"Check your syntax or make sure you've installed any custom modules into the "
                               f"appropriate locations."))
            # if errors:
                # break
        if not errors:
            self.root.instructions += self.create_instruction()
        return errors

    def get_instructions(self):
        return list(self.instructions) + self.generate_first_start()

    def execute_instructions(self):
        for instruction in self.get_instructions():
            instruction.execute(self)

    class NotAModuleException(Exception):
        pass


class ListModule(BaseModule):
    def __init__(self, recipe, parent=None):
        self.list = []
        super().__init__(recipe, parent)

    def handle_modules(self):
        for item in self.recipe:
            self.list.append(item)
        self.root.instructions += self.create_instruction()
        return []


class LocaleModule(BaseModule):
    default_modules = []
    default_fields = ['kb', 'locale', 'timezone', 'wifi']
    module_name = 'locale'

    def create_instruction(self):
        ins = []
        ins.append(ModifyFile('/etc/locale.gen', rf'# {re.escape(self.locale)}', f'{self.locale}'))
        ins.append(ModifyFile('/etc/default/keyboard', '^XKBLAYOUT=".*"$', f'XKBLAYOUT="{self.kb}"'))
        ins.append(ModifyFile('/etc/wpa_supplicant/wpa_supplicant.conf',
                              '^country=.*$', f'country={self.wifi}'))
        ins.append(SymLinkFile('/etc/localtime', f'/usr/share/zoneinfo/{self.timezone}'))
        return ins


class UsersModule(BaseModule):
    default_modules = []
    module_name = 'users'

    def handle_modules(self, *args, **kwargs):
        uid = 1000
        errors = []
        for username in self.recipe:
            if username.startswith('_'):
                continue
            if isinstance(self.recipe[username], str):
                self.recipe[username] = {'pass': self.recipe[username]}
            user = UserModule(username, self.recipe[username], uid, parent=self)
            uid += 1
            errors += user.handle_modules()
        return errors


class UserModule(BaseModule):
    required_fields = []
    default_modules = []
    default_fields = ['uid', 'gid', 'shell', 'name']

    def __init__(self, username, recipe, uid, *args, **kwargs):
        super().__init__(recipe, *args, **kwargs)
        self.module_name = username
        self.username = username
        self.password = "*"
        self.name = username
        self.ssh = False
        self.shell = "/bin/sh"
        self.uid = uid
        self.gid = 1000

    def field_pass(self, recipe, *args, **kwargs):
        if recipe == "LOCKED":
            self.password = f"*LOCKED*{crypt.crypt(recipe)}"
        elif recipe == "NOPASS":
            self.password = "*"
        else:
            self.password = f"{crypt.crypt(recipe)}"
        return []

    def field_ssh(self, recipe, *args, **kwargs):
        if os.path.exists(recipe):
            return [f"{self.__class__.__name__}.ssh does not exist."]
        elif os.access(recipe, os.R_OK):
            return [f"{self.__class__.__name__}.ssh cannot be read."]
        self.ssh = recipe
        return []

    def create_instruction(self):
        ins = []
        passwd = self.root.recipe['install']['users']['_passwd']
        shadow = self.root.recipe['install']['users']['_shadow']
        if self.username in self.root.recipe['install']['users']['_default_users']:
            ins.append(ModifyFile(passwd, rf'^{self.username}:(.+):(.+):(.+):(.+):(.+):(.+)$',
                                          (rf'{self.username}:x:{self.uid}:{self.gid}:'
                                           rf'{self.name},,,:\4:{self.shell}')))
            ins.append(ModifyFile(shadow, rf'^{self.username}:(.+):(.+):(.+):(.+):(.+):(.+)$',
                                          (rf'{self.username}:{self.password}:{self.uid}:{self.gid}:'
                                           rf'{self.name},,,:\4:{self.shell}')))
        else:
            ins.append(CreateDir(f"/home/{self.username}", self.uid, self.gid, 0o775))
            ins.append(ModifyFile(passwd, r'\Z', (rf'\n{self.username}:x:{self.uid}:{self.gid}:'
                                                  rf'{self.name},,,:/home/{self.username}:{self.shell}')))
            ins.append(ModifyFile(shadow, r'\Z', rf'\n{self.username}:{self.password}:17766:0:99999:7:::'))
            if self.shell != "/bin/sh":
                self.add_program(os.path.basename(self.shell))
        if self.ssh:
            ins.append(CreateDir(f"/home/{self.username}/.ssh", self.uid, self.gid, 0o700))
            fname = os.path.basename(self.ssh)
            ins.append(CopyFile(self.ssh, f"/home/{self.username}/.ssh/{fname}", self.uid, self.gid, 0o600))
        return ins


class DNSModule(BaseModule):
    nameserver = ['8.8.8.8', '8.8.4.4']
    default_fields = ['domain', 'nameserver']
    module_name = 'dns'

    def create_instruction(self):
        return [ModifyFile('/etc/dhcpcd.conf',
                           r'\Z', (rf'\nstatic domain_name_servers={" ".join(self.nameserver)}'
                                   rf'\nstatic domain_search={self.domain}')), ]


class WifiModule(BaseModule):
    module_name = 'wifi'
    default_fields = ['psk', 'ssid']

    def create_instruction(self):
        f = '/etc/wpa_supplicant/wpa_supplicant.conf'
        if self.psk and self.ssid:
            return [ModifyFile(f, r'\Z', rf'\nnetwork={{\n\tssid="{self.ssid}"\n\tpsk="{self.psk}"\n}}"'), ]
        else:
            return []


class NetworkModule(BaseModule):
    default_modules = [DNSModule, WifiModule]
    module_name = 'network'
    default_fields = ['hostname', 'ntp']

    def create_instruction(self):
        ins = []
        ins.append(ModifyFile('/etc/hostname', r'.*', self.hostname))
        ins.append(ModifyFile('/etc/hosts', r'^127\.0\.1\.1.*$', f'127.0.1.1	{self.hostname}'))
        ins.append(ModifyFile('/etc/systemd/timesyncd.conf',
                              r'\Z', rf'FallbackNTP={" ".join(self.ntp)}'))
        return ins


class BootupModule(BaseModule):
    module_name = 'bootup'
    default_fields = ['netwait', 'splashscreen']

    def create_instruction(self):
        ins = []
        if not self.splashscreen:
            ins.append(ModifyFile('/boot/cmdline.txt', r' ?splash', ''))
        else:
            ins.append(CopyFile(self.splashscreen, '/usr/share/plymouth/themes/pix/splash.png'))
        if self.netwait:
            ins.append(CreateFile('/etc/systemd/system/dhcpcd.service.d/wait.conf',
                                  """[Service]\nExecStart=\nExecStart=/usr/lib/dhcpcd5/dhcpcd -q -w"""))
        return []


class InstallModule(BaseModule):
    default_modules = [UsersModule, NetworkModule, BootupModule, LocaleModule]
    module_name = 'install'


class AptModule(ListModule):
    default_modules = []
    module_name = 'apt'

    def create_instruction(self):
        for item in self.list:
            self.root.add_program(item)
        return []


class ServicesModule(BaseModule):
    default_modules = [AptModule, ]
    module_name = 'services'


class ImageURLModule(BaseModule):
    default_modules = []
    module_name = 'image-url'

    def handle_modules(self):
        # stub module. this doesn't actually inherantly do anything... yet.
        return []


class PostModule(BaseModule):
    default_modules = []
    module_name = 'post'
    shell_commands = []
    files = {}

    def field_run(self, recipe, *args, **kwargs):
        for action in recipe:
            self.shell_commands.append(action)
        return []

    def field_files(self, recipe, *args, **kwargs):
        for file_dict in recipe:
            for key in file_dict:
                self.files[os.path.abspath(key)] = file_dict[key]
        return []

    def create_instruction(self):
        ins = []
        for action in self.shell_commands:
            self.root.first_start.append(action)
        for local_file in self.files:
            ins.append(CopyFile(local_file, self.files[local_file]))
        return ins


def progress_bar(block_num, block_size, total_size):
    console_width = os.get_terminal_size().columns
    width = console_width - 10
    percent = (block_num * block_size) / total_size
    progress = '#' * int(percent * width) + ' ' * (width - int(percent * width))
    percent_done = str(int(percent * 100)).rjust(3) + "%"
    print(f"\r[ {progress} {percent_done} ]", end='')


class Module(BaseModule):
    def __init__(self, config, parent=None):
        self.config = config
        super().__init__(config.data, parent)
        self.register_module(InstallModule)
        self.register_module(PostModule)
        self.register_module(ImageURLModule)
        self.register_module(ServicesModule)
        self.rootfs = ''
        self.partitions = []
        self.boot_part = []
        self.fstab = []
        self.unmountables = []

    def _get_system_path(self, *path):
        return os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', *path)

    def _fdisk(self):
        partitions = {}
        with open(self.image_path, 'rb') as f:
            mbr = f.read(512)
        if not mbr[-2:] == b'U\xaa':  # 0x55 0xAA
            raise OSError("Invalid or corrupted primary image.")
        UUID = mbr[440:444][::-1].hex()
        part_entries = mbr[446:-2]  # 64 bytes, 4x 16byte partition entries.
        part_entries = [part_entries[start:start + 16] for start in range(0, 64, 16)]
        part_i = 1
        for part_entry in part_entries:
            uuid = f"{UUID}-{part_i:0>2}"
            names = ['status', 'start_CHS', 'type', 'end_CHS', 'start_sector', 'num_sectors']
            partition_info = struct.unpack('<b3sb3sii', part_entry)
            partition_info = dict(zip(names, partition_info))
            if partition_info['num_sectors'] > 0:
                partitions[uuid] = partition_info
            if part_i == 1:
                self.boot_part = partition_info
                if partition_info['type'] == 0xEE:
                    raise OSError("GPT partitioning found. Only MBR is currently supported.")
            part_i += 1

        self.partitions = partitions

    def path(self, full_path):
        if full_path.startswith('~'):
            OSError("Destination path {full_path} cannot include a ~ character- target user is ambiguous.")
        return os.path.join(self.rootfs, full_path.lstrip(os.path.sep))

    def _mount_get_image(self):
        image_url = self.recipe['image-url']
        image_name = Path(image_url).stem
        local_images_folder = self._get_system_path('images')

        if not os.path.exists(local_images_folder):
            os.mkdir(local_images_folder)

        self.image_path = self._get_system_path('images', image_name + ".img")
        if not os.path.exists(self.image_path):
            print(f"Downloading {image_name}...")
            filename, headers = urllib.request.urlretrieve(image_url, reporthook=progress_bar)
            print(f"\nUnzipping {image_name}...")
            with zipfile.ZipFile(filename) as zip:
                zip.extractall(local_images_folder)
            os.remove(filename)
        else:
            print(f"Using images/{image_name}.img")

        os.getcwd()
        new_path = os.path.join(os.getcwd(), f"{self.config.filename.rsplit('.', 1)[0]}.img")
        print('Creating image...')
        try:
            os.remove(new_path)
        except FileNotFoundError:
            pass
        copy(self.image_path, new_path)
        self.image_path = new_path

    def _bootloader(self):
        # Temp mount the first partition. This (should) be /boot, and contain some reference to
        # kernel arguments via the bootloader. With an rpi, that's cmdline.txt.
        root = ''
        offset = 512 * self.boot_part['start_sector']
        sizelimit = 512 * self.boot_part['num_sectors']
        try:
            initfs = tempfile.mkdtemp()
            options = f"loop,rw,offset={offset},sizelimit={sizelimit}"
            cmd = f"/bin/mount -t auto -o {options} {self.image_path} {initfs}"
            # print(cmd)
            os.system(cmd)
            # Now we have to figure out what kind of bootloader is being used.
            if os.path.exists(os.path.join(initfs, "cmdline.txt")):
                with open(os.path.join(initfs, "cmdline.txt")) as f:
                    cmdline = f.read().split(' ')
                    cmd = {arg.split('=')[0]: arg.split('=')[-1] for arg in cmdline}
                    root = cmd['root']
            else:
                raise OSError("Unknown bootloader. Cannot resolve rootfs.")
        finally:
            os.system(f"/bin/umount {initfs}")
            os.rmdir(initfs)
        self.root_part = root

    def _rootfs(self):
        self.rootfs = tempfile.mkdtemp()
        fstab = {}

        if self.root_part in self.partitions:
            partition = self.partitions[self.root_part]
            offset = 512 * partition['start_sector']
            sizelimit = 512 * partition['num_sectors']
            options = f"loop,rw,offset={offset},sizelimit={sizelimit}"
            cmd = f"/bin/mount -t auto -o {options} {self.image_path} {self.rootfs}"
            # print(cmd)
            os.system(cmd)
            self.unmountables.append(self.rootfs)
            with open(os.path.join(self.rootfs, "etc/fstab"), ) as f:
                for line in f.readlines():
                    if line.strip()[0] == "#" or line.strip() == "":
                        continue
                    # device, mount_point, fs, options, dump, pass
                    names = ['device', 'mount_point', 'fs', 'options', 'dump', 'fspass']
                    fstab_args = line.split()
                    fstab_args = dict(zip(names, fstab_args))
                    fstab[fstab_args['device']] = fstab_args
        else:
            raise OSError(f"Kernel rootfs {self.root_part} not found in partition table.")
        self.fstab = fstab

    def _finish_fs(self):
        for device in self.fstab:
            if device.startswith('PARTUUID='):
                PARTUUID = device.replace('PARTUUID=', '', 1)
                if PARTUUID == self.root_part:
                    continue  # This is already mounted, skip.
                if PARTUUID in self.partitions:
                    partition = self.partitions[PARTUUID]
                    offset = 512 * partition['start_sector']
                    sizelimit = 512 * partition['num_sectors']
                    options = f"loop,rw,offset={offset},sizelimit={sizelimit}"
                    mount_point = os.path.join(self.rootfs,
                                               self.fstab[device]['mount_point'].lstrip(os.path.sep))
                    cmd = f"/bin/mount -t auto -o {options} {self.image_path} {mount_point}"
                    # print(cmd)
                    os.system(cmd)
                    self.unmountables.append(mount_point)
            else:  # Skip it, it's a tmpfs, proc, or something else unsupported for now.
                pass

    def mount(self):
        self._mount_get_image()
        self._fdisk()
        self._bootloader()
        self._rootfs()
        self._finish_fs()
        print(self.rootfs)

    def unmount(self):
        for folder in self.unmountables[::-1]:
            os.system(f"/bin/umount {folder}")
            os.rmdir(folder)
