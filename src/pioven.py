#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import sys

import config
from default_modules.base import Module


def prechecks():
    # Check that the User is Root
    if os.geteuid() != 0:
        sys.exit('You must be root to run this program!')

    # Check that we're on ubuntu ( for now )
    opsys = os.uname()
    opsys = opsys.version

    if 'Ubuntu' in opsys:
        print('User is running on Ubuntu... Continuing...')

    elif 'Ubuntu' not in opsys:
        print(f'Current OS Version: {opsys}')
        sys.exit('You must run this script on Ubuntu!')


def main():

    prechecks()

    argparser = argparse.ArgumentParser()

    argparser.add_argument('--config', help='The pie.conf file to use')
    argparser.add_argument('--dry-run', action='store_true')

    args = argparser.parse_args()

    if args.config:
        cfg = config.ParseConfig(args.config)
        print(f'User specified configuration file {cfg}')
        print(cfg.dumps())
        recipe = Module(cfg)
        errors_list = recipe.handle_modules()
        for error in errors_list:
            print(error)
        print()
        for instruction in recipe.get_instructions():
            print(instruction)
        print('')

        if args.dry_run:
            sys.exit()

        try:
            recipe.mount()
            input("DEBUG: Press enter to execute instructions. ")
            recipe.execute_instructions()
            print('Instructions executed!')
        finally:
            input("DEBUG: Press enter to unmount and quit. ")
            print("DEBUG: Unmounted.")
            recipe.unmount()

    elif not args.config:
        sys.exit('Please specifiy a configuration file with --config')


if __name__ == '__main__':
    main()
