#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import copy


def get_list(obj):
    """ Many things in our recipes can be either a value or a list of values. get_list allows us to
        always treat these values as a list. """
    if isinstance(obj, list):
        return obj
    else:
        return [obj, ]


def merge_yaml(original, new):
    """ Do a non-destructive deep merge of two YAML configs. The second config will be preferred,
        and any lists will be concatenated, unless the list is suffixed with a bang (!).
        In that case, the list of the first is replaced with the list of the second."""
    merged = copy.deepcopy(original)
    for orig_obj in new:
        obj = orig_obj.rstrip('!')
        if obj in merged:
            if isinstance(original[obj], dict):
                merged[obj] = merge_yaml(original[obj], new[orig_obj])
            elif isinstance(original[obj], list):
                if orig_obj[-1] == '!':
                    merged[obj] = get_list(new[orig_obj])
                else:
                    merged[obj] = get_list(original[obj]) + get_list(new[orig_obj])
            else:
                merged[obj] = new[orig_obj]
        else:
            merged[obj] = new[orig_obj]
    return merged
